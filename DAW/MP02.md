# DAW/MP2 – Bases de dades

L’objectiu d’aquest mòdul és formar a l’alumnat en les Bases de Dades de tipus
Relacionals, agafant PostgreSQL com a exemple pràctic.

Es fa servir documentació pròpia desenvolupada pel professorat i, a més a més,
la documentació oficial de PostgreSQL disponible a la seva web. 

L’última UF està dedicada a fer una introducció a les Bases de Dades
documentals, en concret MongoDB, ja que és la BBDD no relacional més
utilitzada.

## UF 1: Introducció a les bases de dades

Es fa una introducció als diferents tipus de Bases de Dades existents,
focalitzant l’atenció en les BBDD Relacionals.  Desprès s’explica i es treballa
el Disseny Conceptual de BBDD i, per últim, la transformació al Disseny Lògic

## UF 2: Llenguatges SQL: DML i DDL

Aquesta UF està dedicada, per una banda, al DML (Data Manipulation Language).
És a dir, les operacions de consulta, inserció, modificació i esborrat de
dades).  Per l’altra banda, el DDL (Data Definition Language) consisteix en la
creació d’estructures de BBDD: creació de BBDD, taules, índexs, vistes, etc.

## UF 3: Llenguatges SQL: DCL i extensió procedimental

Durant aquesta UF es treballen quatre conceptes:

1. Connexions remotes
2. DCL: gestió de permisos i roles (usuaris)
3. Transaccions i bloquejos
4. PLpgSQL: creació de funcions i triggers

## UF 4: Bases de dades objecte-relacionals

En aquesta UF, com ja s’ha comentat a la introducció del mòdul, es fa una
aproximació a les BBDD de tipus documentals, en concret MongoDB.  Per tant,
desprès d’una introducció teòrica, es practicaran les consultes i manipulació
de dades (DML).
