# Desenvolupament d’aplicacions web

Mòdul  | Nom mòdul  | Curs  | Hores | Responsable
-------|------------|-------|-------|-----------
01     | Sistemes informàtics | Primer | 198 | Julio Amorós
02     | Bases de dades | Primer | 231 | Jordi Andúgar
03     | Programació | Primer + Segon | 297 | Rafael Botey (UF1, UF2, UF3, UF4) + Josep Méndez (UF5, UF6)
04     | Llenguatges de marques i sistemes de gestió d’informació | Primer | 99 | Montse Soler
05     | Entorns de desenvolupament | Segon | 66 | Eduard Martínez
06     | Desenvolupament web en entorn client | Segon | 165 | Carlos Val 
07     | Desenvolupament web en entorn servidor | Segon | 165 | Víctor Marquina
08     | Desplegament d’aplicacions web | Segon | 99 | Josep Méndez
09     | Disseny d’interfícies web | Segon | 99 | Eduard Martínez
10     | Formació i orientació laboral | Primer | 99 | Pilar Fandos
11     | Empresa i iniciativa emprenedora | Primer | 66 | Pilar Fandos
12     | Projecte de desenvolupament d’aplicacions web | Segon | 120 | ⅀
13     | Formació en centres de treball | Segon | 317 | Carlos Val 

### Casos peculiars (FOL, FCT, etc.)

* MP10
* MP11
* MP12
* MP13
