# DAW/MP7 – Desenvolupament web en entorn servidor

# Desenvolupament web en entorn servidor

## UF 1: Programació web en entorn servidor

* JAVA-EE amb Postgres. Creació d'aplicacions web utilitzant _servlets_, _beans_,
  JSP i accedint a base de dades.
* Configuració del servidor Glassfish pel desplegament de les aplicacions.
* Estructura d'una aplicació web, patrons de disseny MVC, DAO.
* Seguretat en l'aplicació web, sql injection, cross site scripting amb Java i
  la seguretat de les dades.

## UF 2: Generació dinàmica de pàgines web

* Creació d'aplicacions web mitjançant Apache, PHP i MySQL.
* Configuració del servidor apache pel desplegament de les aplicacions.
* Estructura d'una aplicació web, patrons de disseny MVC.
* Seguretat en l'aplicació web, sql injection, cross site scripting amb php i
  la seguretat de les dades.

## UF 3: Tècniques d'accés a dades

* Creació d'aplicacions web mitjançant _servlets_, _beans_, _framework_ Struts i
  Hibernate.

## UF 4: Serveis web. Pàgines dinàmiques interactives. Webs híbrids

* Creació d'aplicacions web mitjançant una base de dades NoSQL, MongoDB i
  NodeJS.
* Seguretat en l'aplicació web amb NodeJS i la seguretat de les dades.

