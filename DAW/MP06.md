# DAW/MP6 – Desenvolupament web en entorn client

## UF 1: Sintaxi del llenguatge. Objectes predefinits del llenguatge.

Utilitzarem el llenguatge javascript per desenvolupar l’assignatura. Revisarem
les carácterísticas bàsiques i avançades del llenguatge, la sintáxi i els
objectes que ens subministra per desenvolupar el nostre treball i la seva
organització dins d’un projecte.

## UF 2: Estructures definides pel programador. Objectes 

En aquesta segona part revisarem els objectes creats pel programador, els
arrays i les funcions.  Amb aquestes eines muntarem els programes i estudiarem
els avantatges d’utilitzar-les correctament.

## UF 3: Esdeveniments. Manejament de formularis. Model d’objectes del document

Amb la llibreria Jquery revisarem l’estructura, que té forma d’arbre, de la
pagina web i manipularem la seva informació utilitzant aquesta potent llibreria
que permet estandaritzar tots aquests processos

## UF 4: Comunicació asíncrona client-servidor

Quan actualitzem la informació d’una pàgina web, podem procedir de dues formes
per visualitzar la nova informació: recarregant totalment la pàgina, o només la
part afecta pels canvis, a aquesta tècnica se li coneix amb el nom d’AJAX.  El
programa treballa sota el plànol principal sense bloquejar el programa.

