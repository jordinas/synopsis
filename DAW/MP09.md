# DAW/MP9 – Disseny d’interfícies web

En aquest mòdul s’aprofondeix en el disseny d’interficies, tenint en compte les
guies d’estil, i fent servir CSS, Bootstrap i Sass.

A més a més, es fa un repàs dels conceptes més importants dels elements
multimèdia i com fer-los servir.

Per últim, es treballen els conceptes d’accessibilitat i usabilitat.

## UF 1: Disseny de la interfície. Estils

Aquesta UF és la més important, ja que es fa una introducció teòrica respecte a
les guies d’estil però, inmediatament desprès, es comença a desenvolupar fent
servir, en primer lloc CSS3, i desprès Bootstrap.

Per últim, es fa una introducció ràpida a Sass.

## UF 2: Elements multimèdia: creació i integració

En primer lloc es fa una introducció teòrica als conceptes de llicències: tant
de copyright, com copyleft i domini públic.

Desprès s’expliquen els elements multimèdia més importants (imatges, só i
video) i com inserir-los de manera òptima segons les necessitats.

En concret:

1. Imatges: formats i conceptes més importants, optimització fent servir CSS Sprites
2. Só: formats i conceptes més importants, inserció fent servir HTML5
3. Video: formats i conceptes més importants, streaming i inserció fent servir HTML5

## UF 3: Accessibilitat i usabilitat

Ja que aquests dos conceptes estan molt relacionats, es treballen de manera
conjunta.  Per tant, s’expliquen les certificacions WCAG i les seves pautes,
tenint en compte els diferents nivells.

També es veu com validar codi HTML i
CSS.

Per últim, es veuen els principis més importants de l’usabilitat

