# Desenvolupament d’interfícies

## UF 1: disseny i implementació d’interfícies

En aquesta unitat formativa veurem:

+ Conceptes fonamentals i estils d'interacció amb el computador. 
+ Aprendre a implementar l'usabilitat i accessibilitat dins dels nostres programes.
+ Guies d’estil o com donar coherència a la presentació dels nostres formularis.
+ Disseny d'interficies programant-les i amb ajuda d'IDES(programes)

## UF 2: preparació i distribució d’aplicacions

Es revisa alguna de les formes de les quals disposem per distribuir les nostres aplicacions 
i de la realització de proves prèvies per assegurar el correcte funcionament dels múdulos que es vam crear. 
