# Desenvolupament d’aplicacions multiplataforma

Mòdul  | Nom mòdul  | Curs  | Hores | Responsable
-------|------------|-------|-------|-----------
01     | Sistemes informàtics | Primer | 198 | Vicent Fornés 
02     | Bases de dades | Primer | 198 | Dan Triano 
03     | Programació | Primer + Segon | 295 | Ramon Bruballa (UF1, UF2, UF3, UF4) + Ginés Martínez (UF5, UF6)
04     | Llenguatges de marques i sistemes de gestió d’informació | Primer | 99 | Ginés Martínez
05     | Entorns de desenvolupament | Segon | 112 | Carlos Val
06     | Accés a dades | Segon | 104 | Ramon Bruballa
07     | Desenvolupament d’interfícies | Segon | 112 | Carlos Val 
08     | Programació multimèdia i dispositius mòbils | Segon | 112 | Josep Méndez
09     | Programació de serveis i processos | Segon | 102 | Ginés Martínez
10     | Sistemes de gestió empresarial | Segon | 66 | Josep Méndez
11     | Formació i orientació laboral | Primer | 99 | Asunción Vázquez
12     | Empresa i iniciativa emprenedora | Primer | 66 | Valentina López
13     | Projecte de desenvolupament d’aplicacions multiplataforma. | Segon | 120 | ⅀ 
14     | Formació en centres de treball | Segon | 317 | Ginés Martínez

### Casos peculiars (FOL, FCT, etc.)

* MP11
* MP12
* MP13
* MP14
