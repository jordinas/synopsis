# Sistemes de gestió empresarial

Aquest mòdul es centra en veure diferents tecnologies que permetin crear i entendre com funcionen aplicacions multiusuari amb persistència de dades, com pot ser un sistema ERP-CRM.
S'entra en detall en la interacció entre una aplicació d'escriptori o mòbil Android amb un servidor, així com en documentar aquesta interacció.
Finalment es desenvolupa una aplicació on es poden aplicar tots els coneixements adquirits durant el curs.
També es veuen els principals conceptes dels sistemes ERP-CRM comercials.

## UF 1: sistemes ERP-CRM. Implantació

A aquesta primera unitat formativa s'expliquen els conceptes, serveis i tecnologies bàsiques que es fan servir en la part del servidor. Es comença veient un sistema de cues com ActiveMQ per gestionar la difusió de missatges entre components implementats fent servir diferents tecnologies, així com gestionar la desconnexió dels components.
Desprès s'explica com poder oferir les dades del servidor a través d'Internet mitjançant servlets.
Finalment es repassen diferents sistemes de persistència de dades, com les bases de dades.

## UF 2: sistemes ERP-CRM. Explotació i adequació

A aquesta segona unitat formativa s'aprofundeix en l'enteniment de les tecnologies de servidor i en com oferir les dades mitjançant una API. També s'entra en detall en el consum de les dades oferides pel servidor des d'una aplicació mòbil.
Per això, primer es veuen exemples d'APIs, per desprès entrar en detall en com documentar-ne una mitjançant l'eina swagger. Desprès, s'implementa aquesta API mitjançant les tecnologies vistes a la unitat formativa anterior (servlets + bases de dades). Posteriorment es veu com consumir aquests serveis amb API pública des d'Android.
Per consolidar els coneixements adquirits durant tot el curs es fa una pràctica que consisteix en una part de servidor que ofereix dades persistents mitjançant una API, i una part Android que consumeix i modifica aquestes dades.

Finalment es revisen els conceptes dels sistemes ERP-CRM i s'enllacen amb les tecnologies vistes durant el curs.
