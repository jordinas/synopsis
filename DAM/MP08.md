# Programació multimèdia i dispositius mòbils

## UF 1: desenvolupament d’aplicacions per a dispositius mòbils

## UF 2: programació multimèdia

## UF 3: desenvolupament de jocs per a dispositius mòbils

En aquest mòdul aprendràs a crear aplicacions natives en Android per dispositius mòbils. Farem servir l'entorn de desenvolupament Google Studio i instalaràs les aplicacions tant a dispositius virtuals com físiques.

També repasarem com crear aplicacions multimèdia, amb control de l'audio, video i la imatge. Veurem l'API Camera 2, que ens permet tenir un control acurat de la càmera del teu dispositiu.

Finalment, aprendràs a crear jocs per diferents plataformes si bé ens centrarem en la plataforma Android.
