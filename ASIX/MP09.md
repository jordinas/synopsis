# ASI/MP9 – Implantació d’aplicacions web

En aquest mòdul cada UF s’organitza amb una activitat per cada RA.
Les dues UF d’aquest mòdul son força independents, presentant la primera els
elements bàsics de la programació de les aplicacions Web en el servidor, mentre que la segona
introdueix les tècniques actuals per la publicació de continguts estàtics a la Web.

Tot el material de l’assignatura està disponible en el lloc
<https://fadado.github.io/ASIX/MP9/> i duplicat en el repositori local del
servidor del departament.

## UF 1: llenguatges de guions de servidor

Els protocols HTTP i CGI constitueixen els continguts principals d’aquesta UF.
Els alumnes coneixeran i faran senzills scripts CGI amb Python, seguint així
l’esperit del DOGC sense necessitat d’endinsar-se en nous i extensos
ecosistemes com ara PHP, Ruby on Rails, etc. Els alumnes poden per tant ampliar
i amortitzar els seus coneixements de Python sense haver de canviar la
plataforma que ja coneixen.

### A1 – Servidors de pàgines dinàmiques

RA1 de la UF. Tecnologies tractades:

* Configuració de l’entorn de desenvolupament (servidor Web, eines de treball).
* Consulta de documentació sobre els protocols HTTP i CGI.
* Estudi d’scripts CGI ja preparats pel professor.
* Creació de nous scripts CGI.

### A2 – Sistemes de plantilles web

RA2 de la UF. Tecnologies tractades:

* Creació de plantilles amb Genshi.
* Integració de les plantilles amb scripts CGI.
* Processament de formularis.

### A3 – Accés a bases de dades

RA3 de la UF. Per simplificar no usarem cap SGBDR, ja que simples bases de
dades **dbm(3)** ens permeten presentar perfectament els conceptes requerits.
Tecnologies tractades:

* Connexió  amb la base de dades.
* Operacions de lectura i escriptura en la base de dades.
* Disseny de formularis per accedir a les dades emmagatzemades

## UF 2: Implantació de gestors de continguts

Per la seva vigència actual, i per continuïtat temàtica amb el MP4, aquesta UF
es dedica als _gestors de continguts estàtics_. Això representa una actualització a la lletra
del DOGC, mentre el seu esperit segueix viu i els alumnes aprenen tècniques que poden aplicar
personalment de forma productiva. Això també permet completar de forma més
coherent el disseny del mòduls MP4 i MP9.


### A1 – Tecnologies de suport

RA1 de la UF. Amb l’intenció final d’usar el gestor de continguts Jekyll,
presentem inicialment tecnologies bàsiques que caldrà utilitzar. Tecnologies tractades:

* Preparació de documents amb Markdown.
* Gestió de repositoris amb `git`.

### A2 – Preparació dels continguts

RA2 de la UF. Tecnologies tractades:

* Gestió de repositoris remots amb Git.
* Allotjament de repositoris a GitLab.

### A3 – Publicació dels continguts

RA3 de la UF. Tecnologies tractades:

* Allotjament de repositoris a GitHub.
* Publicació de pàgines HTML amb GitHub Pages.

### A4 – Gestors de continguts estàtics

RA4 de la UF. Tecnologies tractades:

* El gestor de continguts Jekyll.
* Creació local de llocs web.
* Publicació de llocs web amb Jekyll i GitHub Pages.

