# Administració de sistemes informàtics en xarxa

Mòdul  | Nom mòdul  | Curs  | Hores | Responsable
-------|------------|-------|------ |------------
01     | Implantació de sistemes operatius | Primer | 231 | Eduard Canet
02     | Gestió de bases de dades | Primer/Segon | 132 / 33 | Montse Soler
03     | Programació bàsica | Primer | 165 | Anna Cots
04     | Llenguatges de marques i sistemes de gestió d’informació | Segon | 99 | Joan Ordinas
05     | Fonaments de maquinari | Primer | 99 | Julio Amorós
06     | Administració de sistemes operatius | Segon | 132 | Eduard Canet
07     | Planificació i administració de xarxes | Primer | 165 | Jordi Solà
08     | Serveis de xarxa i Internet | Segon | 99 | Jordi Andugar
09     | Implantació d’aplicacions web | Segon | 66 | Joan Ordinas
10     | Administració de sistemes gestors de bases de dades | Segon | 99 | Montse Soler
11     | Seguretat i alta disponibilitat | Segon | 99 | Eduard Canet
12     | Formació i orientació laboral | Primer | 99 | Amèlia Verdoy
13     | Empresa i iniciativa emprenedora | Segon | 66 | Amèlia Verdoy
14     | Projecte d’administració de sistemes informátics en xarxa | Segon | 99 | ⅀ 
15     | Formació en centres de treball | Segon | 317 | Anna Cots

### Casos peculiars (FOL, FCT, etc.)

* MP12
* MP13
* MP14
* MP15
