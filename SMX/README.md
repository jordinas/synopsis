# Sistemes microinformàtics i xarxes

Mòdul  | Nom mòdul  | Curs  | Hores | Responsable
-------|------------|-------|-------|-----------
01     | Muntatge i manteniment d’equips | Primer | 198 | Vicent Fornés / Sergio Garrido 
02     | Sistemes operatius monolloc | Primer | 132 | Jordi Andúgar / Jordi Solà
03     | Aplicacions ofimàtiques | Primer | 165 | Jonatan Giménez / Dan Triano 
04     | Sistemes operatius en xarxa | Segon | 132 | Víctor Marquina
05     | Xarxes locals | Primer | 165 | Rafel Botey / Jordi Solà 
06     | Seguretat informàtica | Segon | 132 | Jonatan Giménez
07     | Serveis de xarxa | Segon | 165 | Maribel Espada
08     | Aplicacions web | Segon | 198 | Eduard Martínez
09     | Formació i orientació laboral | Primer | 99 | Susana Cano / Teresa Vengut
10     | Empresa i iniciativa emprenedora | Primer | 66 | Susana Cano / Teresa Vengut
11     | Anglès tècnic | Segon | 99 | Beatriz Fernández
12     | Síntesi | Segon | 99 | ⅀
13     | Formació en centres de treball | Segon | 350 | Maribel Espada

## Mòduls redactats

* MP07 - Pablo García

### Casos peculiars (FOL, FCT, etc.)

* MP09
* MP10
* MP11
* MP12
* MP13
