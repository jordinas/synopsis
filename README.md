## sinopsi

[del ll. _synopsis_, i aquest, del gr. _sýnopsis_, íd.]
 	
_f_ **1** Compendi, exposició sintètica i esquemàtica d'una ciència, d'un període
històric o literari, etc., fet de manera que les dades hi poden ésser fàcilment
trobades o confrontades. 
